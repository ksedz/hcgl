#!python
# -*- coding: utf-8 -*-

import web
import views

urls = (
    '/(.*)/', 'views.redirect',
    '/login', 'views.login',
    '/logout', 'views.logout',
    '/registe', 'views.registe',
    '/', 'views.user',
    '/(.*)', 'views.notfd',
)

app = web.application(urls, globals())
session = web.session.Session(app, web.session.DiskStore('sessions'),
						initializer={'user': None})
app.add_processor(views.my_handler)

def get_session():
    if '_session' not in web.config:
        web.config._session = session

if __name__ == '__main__':
    get_session()
    app.run()