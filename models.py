#!python
# -*- coding: utf-8 -*-

from datetime import datetime
from sqlalchemy import create_engine, Table, ForeignKey
from sqlalchemy import Column, Integer, Float, String, Text, DateTime, Date
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///db/hc.sqlite', echo=True)

Base = declarative_base()
metadata = Base.metadata

class User(Base):
    __tablename__ = 'user'
	
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    password = Column(String(50))
    privilege = Column(Integer, default=0)
	
    def __init__(self, name, password):
        self.name = name
        self.password = password
		
    def __repr__(self):
        return '<User ("%s")>' % self.name
		
class Equipment(Base):
    __tablename__ = 'equipment'
	
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    modelnum = Column(String(50))
    standard = Column(String(50))
    price = Column(Float)
    manufacturer = Column(String(50))
    production_date = Column(Date)
    valid_to = Column(Date)
	
    def __init__(self, name, modelnum, standard, price, manufacturer, production_date, valid_to):
        name = self.name
        modelnum = self.modelnum
        standard = self.standard
        price = self.price
        manufacturer = self.manufacturer
        production_date = self.production_date
        valid_to = self.valid_to

    def __repr__(self):
        return '<Equipment ("%s")>' % self.name

class Inventory(Base):
    __tablename__ = 'inventory'
	
    id = Column(Integer, primary_key=True)
    equipment_id = Column(Integer, ForeignKey('equipment.id'))
    amount = Column(Integer)
    date = Column(DateTime, default=datetime.now())
	
    def __init__(self, equipment_id, amount):
        self.equipment_id = equipment_id
        self.amount = amount
		
    def __repr__(self):
        return '<Inventory ("%s")>' % self.id
		
class Enter(Base):
    __tablename__ = 'enter'
	
    id = Column(Integer, primary_key=True)
    equipment_id = Column(Integer, ForeignKey('equipment.id'))
    principal_id = Column(Integer, ForeignKey('user.id'))
    amount = Column(Integer)
    date = Column(DateTime, default=datetime.now())
	
    def __init__(self, equipment_id, principal_id, amount):
        self.equipment_id = equipment_id
        self.principal_id = principal_id
        self.amount = amount
		
    def __repr__(self):
        return '<Enter ("%s")>' % self.id

class Apply(Base):
    __tablename__ = 'apply'
	
    id = Column(Integer, primary_key=True)
    equipment_id = Column(Integer, ForeignKey('equipment.id'))
    applicant_id = Column(Integer, ForeignKey('user.id'))
    answer_id = Column(Integer, ForeignKey('answer.id'))
    amount = Column(Integer)
    commment = Column(Text) 
    status = Column(Integer, default=0)
    date = Column(DateTime, default=datetime.now())
	
    def __init__(self, equipment_id, applicant_id, amount, comment):
        self.equipment_id = equipment_id
        self.applicant_id = applicant_id
        self.amount = amount
        self.comment = comment
		
    def __repr__(self):
        return '<Apply ("%s")>' % self.id
		
class Delivery(Base):
    __tablename__ = 'delivery'
	
    id = Column(Integer, primary_key=True)
    apply_id = Column(Integer, ForeignKey('apply.id'))
    principal_id = Column(Integer, ForeignKey('user.id'))
    date = Column(DateTime, default=datetime.now())
	
    def __init__(self, apply_id, principal_id):
        self.apply_id = apply_id
        self.principal_id = principal_id
		
    def __repr__(self):
        return '<Delivery ("%s")>' % self.id
		
class Need(Base):
    __tablename__ = 'need'
	
    id = Column(Integer, primary_key=True)
    equipment_id = Column(Integer, ForeignKey('equipment.id'))
    applicant_id = Column(Integer, ForeignKey('user.id'))
    answer_id = Column(Integer, ForeignKey('answer.id'))
    amount = Column(Integer)
    commment = Column(Text)
    status = Column(Integer, default=0)
    date = Column(DateTime, default=datetime.now())
	
    def __init__(self, equipment_id, applicant_id, amount, comment):
        self.equipment_id = equipment_id
        self.applicant_id = applicant_id
        self.amount = amount
        self.comment = comment
		
    def __repr__(self):
        return '<Need ("%s")>' % self.id
		
class Answer(Base):
    __tablename__ = 'answer'
	
    id = Column(Integer, primary_key=True)
    principal_id = Column(Integer, ForeignKey('user.id'))
    comment = Column(Text)
    date = Column(DateTime, default=datetime.now())
	
    def __init__(self, applicant_id, comment):
        self.applicant_id = applicant_id
        self.comment = comment
		
    def __repr__(self):
        return '<Answer ("%s")>' % self.id
	
		
if __name__ == '__main__':
    metadata.create_all(engine)
