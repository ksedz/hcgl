#!python
# -*- coding: utf-8 -*-

def user_wrapper(user):
	mainpage = {0:'/', 1:'/admin', 2:'/sysadmin'}
	category = {0:'user', 1:'admin', 2:'sysadmin'}
	user.mainpage = mainpage[user.privilege]
	user.category = category[user.privilege]
	return user